# Systemd unit

## Task

Напишите Systemd unit, который будет обеспечивать запуск сервера Prometheus, поддерживать работу процесса и перезапускать его в случае выхода из строя.
Также необходимо предусмотреть в этом unit возможность перезагрузки конфигурации сервиса без его рестарта. 

## Solution
Написан и протестирован Systemd unit

```
[Unit]
Description=Prometheus Server
Wants=network-online.target
After=network-online.target  # Запускаем сервис только после того как запустилась сеть

[Service]
Type=simple 
User=prometheus
Group=prometheus
Restart=on-failure # Рестарт при ошибке
ExecReload=/bin/kill -s SIGHUP $MAINPID # Для перезагрузки конфигурации prometheus отправляем ему сигнал SIGHUP
ExecStart=/opt/prometheus/prometheus \   # Команда запуска Prometheus
  --config.file=/opt/prometheus/prometheus.yml \ # файл конфигурации
  --storage.tsdb.path=/var/lib/prometheus \  # путь до data storage
  --storage.tsdb.retention.time=30d # Храним данные 30 дней

[Install]
WantedBy=multi-user.target # Добавляем возможность включать автозагрузку prometheus при старте машины
```