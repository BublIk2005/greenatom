# Dockerfile

## Task

Перепишите Dockerfile 
```
FROM ubuntu:20.04 
COPY ./src /app 
RUN apt-get update -y 
RUN apt-get install -y nodejs 
RUN npm install 
ENTRYPOINT ["npm"] 
CMD ["run", "prod"]
```

## Solution

Можно предложить перенести команды apt-get update -y и apt-get install -y nodejs до вызова команды COPY ./src /app , что оптимизирует данный dockerfile, позволит кэшировать команды update и install.

Так же можно отдельно копировать файл с зависимостями перед выполнение npm install, что так же позволит кэшировать команды COPY package.json . и RUN npm install и не выполнять их повторно при изменении файлов проекта
```
FROM ubuntu:20.04 
RUN apt-get update -y 
RUN apt-get install -y nodejs 
COPY package.json .
RUN npm install 
COPY ./src /app 
ENTRYPOINT ["npm"] 
CMD ["run", "prod"]
```