host="https://it.is.mysite.ru"
# Get content from our website
content=$(curl -i $host )
#return error if curl could not resolve host
if [ "$?" -ne 0 ] ; then
    echo "Could not resolve host: $host"
    exit 1
fi
# Check if there is a phrase "Про важное" in the content
count=$(echo $content | grep -ioc "Про важное")
# Get response code
status_code=$(echo $content | awk '/^HTTP/{print $2}')
# If code not equal 200 - print message
if [ "$status_code" -ne 200 ] ; then
  echo "Site status changed to $status_code"
else
    # If there is no phrase "Про важное"
    if [ $count -eq 0 ] ; then
        echo 'The site does not contains the text "Про важное"'
    # If we get phrase "Про важное"
    else
        echo 'The site contains the text  "Про важное"'
    fi
fi