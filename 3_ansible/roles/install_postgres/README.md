Install Postgres
=========

The role for installing postgresql on Debian 11 host

Role Variables
--------------

postgres_version : 16

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Setup Postgresql server
      hosts: [hosts]
      become: yes
      roles:
        - install_postgresql

