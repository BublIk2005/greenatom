Install Prometheus
=========

The role for installing prometheus on a debian 11 host

Role Variables
--------------

prometheus_version : 2.53.0

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Setup Prometheus server
      hosts: [hosts]
      become: yes
      roles:
        - install_prometheus
