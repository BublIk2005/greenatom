# Ansible

## Task

Напишите простой ansible-скрипт по развороту Prometheus сервера с БД postgres на debian 11 c комментариями для выбранных шагов

## Solution

Разработаны две Ansible-роли для базовой установки Prometheus и PostgreSQL

Точка входа - start_role.yaml

Команда для запуска - ansible-playbook start_role.yaml